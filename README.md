To run,
```
git clone git@bitbucket.org:brandonwoo/scraper.git
cd scraper
bundle
scraper scrape <city name> ..<query string>
```


Examples

Search Vancouver for tatamis
```
ruby scraper.rb scrape vancouver tatami
```

Search New York For fugu
```
ruby scraper.rb scrape New York fugu
```