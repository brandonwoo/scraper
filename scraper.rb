#!/usr/bin/env ruby
require 'rubygems'
require 'commander/import'
require 'yelp'
require 'pp'
require 'titleize'

# these values should be in env or private config file,
# but for convenience purposes for this assignment,
# I just created a throw away yelp account to use for this task
client = Yelp::Client.new({
  consumer_key: "ntBQ__WfoOXK2HUdR_tQDQ",
  consumer_secret: "R2WmtBkvc4tc1raiB2bczqxi30Y",
  token: "-fIAfud85WwX6j-rUIDI5jmB6a6CtC05",
  token_secret: "wbNCA_E_yQ2tjiDP1eRLdUhYar4"
})

program :version, '0.0.1'
program :description, 'ads scraper'
 
command :scrape do |c|
  c.syntax = 'scraper scrape <city name> ..<query string>'
  c.example 'scrape for posts containing "fugu" in San Francisco', 'scraper scrape "San Francisco" fugu'

  c.action do |args, options|
    BATCH_LIMIT = 20

    if args.length < 1
      abort "Please specify city to search in"
    end
    
    city = args.shift
    if args.length < 1
      abort 'Please specify query string(s) to search for'
    end

    query_string = args.join(" ")

    #search yelp
    results = get_results(client, city, query_string, BATCH_LIMIT, 0, [])

    #print resulting hash
    pp results
  end
end

# recursive function that traverses through all matches for query,
# and returns an organized hash of the result
def get_results(client, city, query_string, page_size, offset, results)
  params = {
    term: query_string,
    limit: page_size,
    offset: offset * page_size
  }

  begin
    response = client.search(city, params)
  rescue => e
    puts "yelp client error"
    pp e
    abort
  end

  remaining_results = response.total - (page_size * offset)

  if offset == 0 #output initial info
    puts "Total number of results: #{response.total}"
  end

  number_to_display = remaining_results >= page_size ? page_size : remaining_results
  puts "Traversing next #{number_to_display} results..."

  response.businesses.each { |business|
    result = Hash.new
    result[:name] = business.respond_to?('name') ? business.name : ""
    result[:phone] = business.respond_to?('phone') ? business.phone : ""
    result[:address] = business.respond_to?('location') &&
              business.location.respond_to?('display_address') &&
              business.location.display_address.length >= 1 ?
              business.location.display_address[0] : ""
    results.push(result)
  }

  if remaining_results - page_size > 0
    get_results(client, city, query_string, page_size, offset+1, results)
  else
    puts "Traversed #{results.length} results"
    results
  end
end